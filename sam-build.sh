#!/bin/bash
args="--disable-shared \
      --enable-static \
      --disable-alsa \
      --disable-nls \
      --disable-x11 \
      --disable-tests \
      --disable-esound \
      --disable-waveout \
      --disable-gconf \
      --disable-glib2 \
      --disable-gtk3 \
      --disable-jack \
      --disable-asyncns \
      --disable-avahi \
      --disable-openssl \
      --disable-tcpwrap \
      --disable-lirc \
      --disable-dbus \
      --disable-udev \
      --disable-bluez5 \
      --disable-hal-compat \
      --disable-ipv6 \
      --disable-webrtc-aec \
      --disable-systemd-daemon \
      --disable-systemd-login \
      --disable-systemd-journal \
      --disable-manpages \
      --disable-default-build-tests \
      --disable-legacy-database-entry-format \
      --enable-static-bins=yes \
      --enable-neon-opt=no \
      --without-caps \
      --without-fftw \
      --without-speex \
      --without-soxr \
      --with-database=simple"

# fk-pulseaudio>> static:
export CFLAGS="-Os -fomit-frame-pointer"
# export CFLAGS="-Os -fomit-frame-pointer -march=armv7-a" ##armv7-a armv8-a
# export CFLAGS="$CFLAGS -mfpu=neon" #开/注释
export CXXFLAGS="$CFLAGS"
export CPPFLAGS="$CFLAGS"
# export LDFLAGS="-Wl,--strip-all -Wl,--as-needed"
export LDFLAGS="-static -Wl,--strip-all -Wl,--as-needed"
export CC=xx-clang
export CXX=xx-clang++

test -z "$CMAKE_BINARY_DIR" && CMAKE_BINARY_DIR=/usr/local/static/pulseaudio
# args="--disable-shared \ ##ref pulse03.md
# -lltdl 
dep1="-logg -lopus -lsndfile  -lFLAC -lvorbis -lvorbisenc"

# # x11-base's call; ##autogen.sh之后即OK (./git-version-gen文件)
# # bash autogen.sh ##直接调用下一行
# NOCONFIGURE=1 ./bootstrap.sh
# # echo "cat git-version-gen"; cat git-version-gen  ##./git-version-gen .tarball-version  ###xx.sh@src
git pull; autoreconf --force --install --verbose 2>&1
# 
git pull; LIBS="$dep1" ./configure     --prefix=${CMAKE_BINARY_DIR}     $args
cat ./src/Makefile  |grep gsett |wc
sed -i "s/LIBLTDL = @LIBLTDL@/LIBLTDL =/g" ./src/Makefile
make clean; make LDFLAGS="-static --static $dep122"



# exit 0
# ###CHECK1#######################################################
# # ogg -lopus -lsndfile  -lFLAC -lvorbis -lvorbisenc

# # find /usr/lib /usr/local/lib |egrep "ogg|opus|sndfile|FLAC|vorbis" |egrep "\.a$" |sort
# /tmp/pulseaudio # find /usr/lib /usr/local/lib |egrep "ogg|opus|sndfile|FLAC|vorbis" |egrep "\.a$" |sort
# /usr/local/lib/libFLAC++.a
# /usr/local/lib/libFLAC.a
# /usr/local/lib/libogg.a
# /usr/local/lib/libopus.a
# /usr/local/lib/libsndfile.a
# /usr/local/lib/libvorbis.a
# /usr/local/lib/libvorbisenc.a
# /usr/local/lib/libvorbisfile.a

# # miss: opus, FLAC
