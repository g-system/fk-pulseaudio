

- ./configure.ac `cnt1620/1782`
  - 各种条件,check定义 (宏定义> autogen.autoreconf生成./configure)
  - ./configure>> Makefile.am >> Makefile;
- 
- ./Makefile.am `cnt150` `common项; 无LDFLAGS`
- ./src/Makefile.am `cnt2226/2322` `LDFLAGS x462`
- ./src/modules/Makefile `daemon,pulse,pulsecore,tests,utils`
  - 基础三步;
  - try `LDFLAGS = -logg -lopus -lsndfile  -lFLAC -lvorbis -lvorbisenc`

```bash
# ./src/Makefile.am:
  # common AM_LDFLAGS/AM_LIBLDFLAGS
  #       Extra directories
  #       Compiler/linker flags
  #       Extra files
  #       Includes
  # pulseaudio_LDFLAGS
  #       Main daemon(L:149)
  # bincmdxx_LDFLAGS
  #       Utility programs(L:194)
  # 
  # 
  # tests_xx
  #         Test programs(L:255)
  # 
  # libpulsecommon:pulse,pulsecore,..
  #         Common library(L:691) << libpulsecommon
  #         Client library(L:858) << libpulse
  #         OSS emulation(L:959)
  #         Daemon core library(L:984) << libpulsecore
  # modules_xx
  #        Plug-in support libraries(L:1128) << libprotocol,librtp,libaop,libavahi
  #        Plug-in libraries(L:1229) << modlibexec, module_simple_protocol_tcp/unix, module_cli_protocol_tcp/unix, module_http_protocol_tcp/unix, module_dbus_protocol, module_native_protocol_tcp/unix/fd, module_esound_protocol_tcp/unix/fd, module_esound_sink, module_pipe_sink/source, module_null_sink/source
  #        Some minor stuff(L:2227..2321end) << cmds


```

**mods**

- module-native-protocol-unix/module-native-protocol-tcp; 加二层build-code开关,免重复
- 
- module-augment-properties
- module-always-sink
- module-x11-publish [ifexists]; autoconf:默认未带其静库
- 
- /var/lib/xrdp-pulseaudio-installer/module-xrdp-[sink/source].so

```bash
# module-x11-publish ##默认未加载a库?; 
# https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules/#module-x11-publish
Publishes the access credentials to the PulseAudio server in the X11 root window.
# The following properties are used: PULSE_SERVER, POYLP_SINK, PULSE_SOURCE, PULSE_COOKIE.
This is very useful when using SSH or any other remote login tool for logging into other machines and getting audio playback to your local speakers.


# module-xrdp-sink/source
# https://gitee.com/g-system/pulseaudio-module-xrdp
# 
# $ ll -h /var/lib/xrdp-pulseaudio-installer/
总用量 168K
-rw-r--r-- 1 root root 83K 11月 15  2022 module-xrdp-sink.so
-rw-r--r-- 1 root root 82K 11月 15  2022 module-xrdp-source.so
# headless @ tenvm2 in .../_misc2/fk-pulseaudio |09:38:17  |detached: ✓| 
$ ldd /var/lib/xrdp-pulseaudio-installer/module-xrdp-sink.so 
	/lib64/ld-linux-x86-64.so.2 (0x00007ff312c8f000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007ff312a8b000)
	linux-vdso.so.1 (0x00007ffe609f0000)
$ ldd /var/lib/xrdp-pulseaudio-installer/module-xrdp-source.so 
	/lib64/ld-linux-x86-64.so.2 (0x00007fad5597c000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fad55778000)
	linux-vdso.so.1 (0x00007fff72761000)

```

**testRun**

```bash
# view
/tmp/pulseaudio # ls -lh ./src/pulseaudio 
-rwxr-xr-x    1 root     root        6.1M Jun 17 06:19 ./src/pulseaudio
/tmp/pulseaudio # ldd ./src/pulseaudio 
/lib/ld-musl-aarch64.so.1: ./src/pulseaudio: Not a valid dynamic program

# run
$ ./src/pulseaudio --log-meta=true --log-time=true --log-level=4  --exit-idle-time=-1 -nF ./_t1/pulse.pa 2>&1 #|grep "=="
```